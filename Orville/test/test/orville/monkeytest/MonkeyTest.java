package test.orville.monkeytest;

import org.javaforever.oville.NarrowWord;
import org.javaforever.oville.StringToHex;
import org.javaforever.oville.Word;

public class MonkeyTest {
	public static String monkeyBit() {
		String [] bits = {"0","1"};
		return bits[(int)(Math.random()*bits.length)];
	}
	public static String monkeyHex() {
		String [] bits = {"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"};		
		return bits[(int)(Math.random()*bits.length)];
	}
	public static String monkeyQuad() {
		String [] bits = {"0","1","Q","P"};		
		return bits[(int)(Math.random()*bits.length)];
	}
	public static String monkeyQuadBin() {
		String [] bits = {"00","01","10","11"};		
		return bits[(int)(Math.random()*bits.length)];
	}
	public static String monkeyQQuad() {
		String [] bits = {"Q","P"};		
		return bits[(int)(Math.random()*bits.length)];
	}
	public static String monkeyQQuadBin() {
		String [] bits = {"10","11"};		
		return bits[(int)(Math.random()*bits.length)];
	}
	public static byte monkeyByte() {		
		StringBuilder sb = new StringBuilder();
		sb.append(monkeyHex()).append(monkeyHex());
		return StringToHex.hexStringToByte(sb.toString());
	}
	public static NarrowWord monkeyNarrowWord(String varName) {
		byte [] bytes = new byte[4];
		for (int i=0;i<4;i++) {
			bytes[i] = monkeyByte();
		}
		NarrowWord nw = new NarrowWord();
		nw.setVarName(varName);
		nw.setWord(bytes);
		return nw;
	}
	public static Word monkeyWord(String varName) {
		byte [] bytes = new byte[8];
		for (int i=0;i<8;i++) {
			bytes[i] = monkeyByte();
		}
		Word w = new Word();
		w.setVarName(varName);
		w.setWord(bytes);
		return w;
	}
	
	public static void main(String [] argv) throws Exception {
		int zero= 0,one= 0,quantum= 0,project = 0;
		for (int i=0;i<1000;i++) {
			String quad = monkeyQuad();
			System.out.println(quad);
			if (quad.equals("0")) zero ++;
			if (quad.equals("1")) one ++;
			if (quad.equals("Q")) quantum ++;
			if (quad.equals("P")) project ++;
		}
		System.out.println("zero:"+zero);
		System.out.println("one:"+one);
		System.out.println("quantum:"+quantum);
		System.out.println("project:"+project);		
	
		for (int i=0;i<1000;i++) {
			String hex = monkeyHex();
			System.out.println(hex);
		}
		
		for (int i=0;i<1000;i++) {
			byte byte0 = monkeyByte();
			System.out.println(byte0);
		}
		
		for (int i=0;i<1000;i++) {
			NarrowWord nw = monkeyNarrowWord("nw");
			System.out.println(nw.toHexStr());
		}
		
		for (int i=0;i<1000;i++) {
			Word w = monkeyWord("word");
			System.out.println(w.toHexStr()+":"+w.toQuadStr());
		}
	}
}
