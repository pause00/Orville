package org.javaforever.oville;

public class Var implements Comparable<Var>{
	private String varName;
	private Words words;
	private NarrowWords narrowWords;
	private String varType = "Words";
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}
	public Words getWords() {
		return words;
	}
	public void setWords(Words words) {
		this.words = words;
	}
	public NarrowWords getNarrowWords() {
		return narrowWords;
	}
	public void setNarrowWords(NarrowWords narrowWords) {
		this.narrowWords = narrowWords;
	}
	public String getVarType() {
		return varType;
	}
	public void setVarType(String varType) {
		this.varType = varType;
	}
	@Override
	public int compareTo(Var o) {
		return this.varName.compareTo(o.getVarName());
	}

}
