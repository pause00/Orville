package org.javaforever.oville;

import java.util.ArrayList;
import java.util.List;

public class Words {
	private String varName;
	private List<Word> words = new ArrayList<Word>();
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}
	public List<Word> getWords() {
		return words;
	}
	public void setWords(List<Word> words) {
		this.words = words;
	}
	public void addWord(Word w) {
		if (this.words != null) this.words.add(w);
		else {
			this.words = new ArrayList<Word>();
			this.words.add(w);
		}
	}
	
	public String toQuadStr() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		for (Word w:this.getWords()) {
			sb.append(w.toQuadStr()).append(":");
		}
		return sb.toString();
	}
}
