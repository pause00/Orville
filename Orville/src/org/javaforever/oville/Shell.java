package org.javaforever.oville;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class Shell {
	private String prompt = "Orville:>";
	private final String defaultPrompt = "Orville:>";
	private InputStream in;
	private PrintStream out;
	private Scanner scanner;
	
	public Shell(InputStream in,PrintStream out) {
		this.in = in;
		this.out = out;
		this.scanner = new Scanner(this.in); ;
	}

	public String getPrompt() {
		return prompt;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}
	
	public String input() {
		this.out.print(this.prompt);
		String input = this.scanner.nextLine();
		this.out.println(input);
		return input;
	}
	
	public void output() {
		
	}

	public InputStream getIn() {
		return in;
	}

	public void setIn(InputStream in) {
		this.in = in;
	}

	public PrintStream getOut() {
		return out;
	}

	public void setOut(PrintStream out) {
		this.out = out;
	}

	public String getDefaultPrompt() {
		return defaultPrompt;
	}

	public Scanner getScanner() {
		return scanner;
	}

	public void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}
}
