package org.javaforever.oville;

import java.util.Map;

public class Action {
	private String actionName;
	private Map<String,String> params;
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public Map<String, String> getParams() {
		return params;
	}
	public void setParams(Map<String, String> params) {
		this.params = params;
	}
}
