package org.javaforever.oville;
public class StringToHex {
 
	/** 
     * 字符串转换为16进制字符串 
     *  
     * @param s 
     * @return 
     */  
    public static String stringToHexString(String s) {  
        String str = "";  
        for (int i = 0; i < s.length(); i++) {  
            int ch = s.charAt(i);  
            String s4 = Integer.toHexString(ch);  
            str = str + s4;  
        }  
        return str;  
    }  
  
    /** 
     * 16进制字符串转换为字符串 
     *  
     * @param s 
     * @return 
     */  
    public static String hexStringToString(String s) {  
        if (s == null || s.equals("")) {  
            return null;  
        }  
        s = s.replace(" ", "");  
        byte[] baKeyword = new byte[s.length() / 2];  
        for (int i = 0; i < baKeyword.length; i++) {  
            try {  
                baKeyword[i] = (byte) (0xff & Integer.parseInt(  
                        s.substring(i * 2, i * 2 + 2), 16));  
            } catch (Exception e) {  
                e.printStackTrace();  
            }  
        }  
        try {  
            s = new String(baKeyword, "gbk");  
            new String();  
        } catch (Exception e1) {  
            e1.printStackTrace();  
        }  
        return s;  
    }  
    
	/**
	 * 16进制表示的字符串转换为字节数组
	 *
	 * @param s 16进制表示的字符串
	 * @return byte[] 字节数组
	 */
	public static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] b = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        // 两位一组，表示一个字节,把这样表示的16进制字符串，还原成一个字节
	        b[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
	                .digit(s.charAt(i + 1), 16));
	    }
	    return b;
	}
	

	public static NarrowWords hexStringToNarrowWords(String s,String varName) {
	    int len = s.length();
	    NarrowWords nws = new NarrowWords();
	    nws.setVarName(varName);
	    int curLen = 0;
	    for (int i=0;i<len;i+=curLen) {
	    	curLen = i==0?len%8:8;
	    	NarrowWord nw = new NarrowWord();
	    	nw.setVarName(varName);
	    	byte [] bytes = hexStringToByteArray(s.substring(i,i+curLen));
	    	byte [] results = {(byte)0,(byte)0,(byte)0,(byte)0};
	    	if (bytes.length == 4) results = bytes;
	    	else if  (bytes.length ==3) {
	    		results[1] = bytes[0];
	    		results[2] = bytes[1];
	    		results[3] = bytes[2];
	    	}
	    	else if  (bytes.length ==2) {
	    		results[2] = bytes[0];
	    		results[3] = bytes[1];
	    	}
	    	else if  (bytes.length ==1) {
	    		results[3] = bytes[0];
	    	}
	    	
	    	nw.setWord(results);
	    	nws.addNarrowWord(nw);
	    }
	    return nws;
	}
	
	/**
	 * 16进制表示的字符串转换为字节数组
	 *
	 * @param s 16进制表示的字符串
	 * @return byte[] 字节数组
	 */
	public static byte hexStringToByte(String s) {
	    int len = s.length();
	    byte b = 0;
	        // 两位一组，表示一个字节,把这样表示的16进制字符串，还原成一个字节
        b = (byte) ((Character.digit(s.charAt(0), 16) << 4) + Character
	                .digit(s.charAt(1), 16));
	    return b;
	}
	
	/**
	 * byte数组转16进制字符串
	 * @param bArray
	 * @return
	 */
	 public static final String bytesToHexString(byte[] bArray) {
		  StringBuffer sb = new StringBuffer(bArray.length);
		  String sTemp;
		  for (int i = 0; i < bArray.length; i++) {
		   sTemp = Integer.toHexString(0xFF & bArray[i]);
		   if (sTemp.length() < 2)
		    sb.append(0);
		   sb.append(sTemp.toUpperCase());
		  }
		  return sb.toString();
		 }
}