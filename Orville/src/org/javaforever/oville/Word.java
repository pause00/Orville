package org.javaforever.oville;

public class Word {
	private String varName;
	private byte [] word = new byte[8];
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}
	public byte[] getWord() {
		return word;
	}
	public void setWord(byte[] word) {
		this.word = word;
	}	
	public String toBinStr() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<8;i++) {
			byte bb = word[i];
			if (bb<0) bb += 256;
			for (int j=7;j>=0;j--) {
				byte bit = (byte)((bb >>> j)%2);
				if (bit<0) bit += 2;
				//System.out.println(bit);
				sb.append(bit);
			}
		}
		return sb.toString();
	}
	
	public String toQuadStr() throws ValidateException{
		String bin = this.toBinStr();
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<32;i++) {
			sb.append(StringUtil.binDigToQuad(bin.substring(i*2,(i+1)*2)));
		}
		return sb.toString();
	}	
	
	public String toHexStr() throws ValidateException{
		String bin = this.toBinStr();
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<16;i++) {
			sb.append(StringUtil.binDigToHex(bin.substring(i*4,(i+1)*4)));
		}
		return sb.toString();
	}	
	public static void main(String[] argv) throws ValidateException{
		Word w = new Word();
		w.setVarName("word");
		byte [] bb = new byte[8];
		bb[0] = (byte)0b01010101;
		bb[1] = (byte)0b10101010;
		bb[2] = (byte)0b10111010;
		w.setWord(bb);
		System.out.println(w.toBinStr());
		System.out.println(w.toHexStr());
		System.out.println(w.toQuadStr());
	}

}
