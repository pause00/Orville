package org.javaforever.oville;

import java.util.Map;
import java.util.TreeMap;

public class Intercepter {
	
	public Action intercepte(Statement st) {
		String input = st.getContent();
		if (input.startsWith("quit")) {
			Action ac = new Action();
			ac.setActionName("quit");
			return ac;
		}
		if (input.startsWith("setPrompt")&& input.split(" ").length >= 2) {
			Action ac = new Action();
			ac.setActionName("setPrompt");
			Map<String,String> params = new TreeMap<String,String>();
			params.put("prompt", input.substring(9,input.length()).trim());
			ac.setParams(params);
			return ac;
		}
		if (input.startsWith("resetPrompt")) {
			Action ac = new Action();
			ac.setActionName("resetPrompt");
			return ac;
		}
		if (input.startsWith("codeWord")&& input.split(" ").length == 2) {
			Action ac = new Action();
			ac.setActionName("codeWord");
			Map<String,String> params = new TreeMap<String,String>();
			params.put("narrowWord", input.split(" " )[1]);
			ac.setParams(params);
			return ac;
		}
		if (input.startsWith("code")) {
			Action ac = new Action();
			ac.setActionName("code");
			Map<String,String> params = new TreeMap<String,String>();
			String [] inputs = input.split("(\\s+=*\\s*|\\s*=+\\s*)");
			if (inputs.length == 2)params.put("narrowWords", input.split(" " )[1]);
			if (inputs.length == 4 && inputs[1].equals("var")) {
				params.put("narrowWords", inputs[3]);
				params.put("varName", inputs[2]);
				params.put(inputs[2], inputs[3]);
			}
			ac.setParams(params);
			return ac;
		}
		if (input.startsWith("decode")) {
			Action ac = new Action();
			ac.setActionName("decode");
			Map<String,String> params = new TreeMap<String,String>();
			String [] inputs = input.split("\\s+");
			if (inputs.length == 3 && inputs[1].equals("var")) {
				params.put("varName", inputs[2]);
			}
			ac.setParams(params);
			return ac;
		}
		if (input.startsWith("monkeyTest.monkeyNarrowWord")) {
			Action ac = new Action();
			ac.setActionName("monkeyTest.monkeyNarrowWord");
			return ac;
		}
		if (input.startsWith("monkeyTest.monkeyWord")) {
			Action ac = new Action();
			ac.setActionName("monkeyTest.monkeyWord");
			return ac;
		}
		return null;
	}
	
	public static void main(String [] argv) {
		String str = "code var a=0x11";
		String [] strs = str.split("(=|\\s)");
		for (String st:strs)
			System.out.println(st);
		
	}

}
