package org.javaforever.oville;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.javaforever.oville.operator.Code;
import org.javaforever.oville.operator.Decode;

import test.orville.monkeytest.MonkeyTest;

public class Core {
	private Shell shell = new Shell(System.in,System.out);
	private Intercepter intercepter = new Intercepter();
	private List<NarrowWord> narrowWords = new ArrayList<NarrowWord>();
	private List<Word> words = new ArrayList<Word>();
	private List<Statement> statements = new ArrayList<Statement>();
	private Map<String,Var> vars = new TreeMap<String,Var>();
  	public Shell getShell() {
		return shell;
	}
	public void setShell(Shell shell) {
		this.shell = shell;
	}
	public List<NarrowWord> getNarrowWords() {
		return narrowWords;
	}
	public void setNarrowWords(List<NarrowWord> narrowWords) {
		this.narrowWords = narrowWords;
	}
	public List<Word> getWords() {
		return words;
	}
	public void setWords(List<Word> words) {
		this.words = words;
	}
	public List<Statement> getStatements() {
		return statements;
	}
	public void setStatements(List<Statement> statements) {
		this.statements = statements;
	}
	
	public static void main(String[] argv) {
		Core core = new Core();
		core.getShell().getOut().println("Orville quatum computer. Welcome!");
		while (true) {
			try {
				String input = core.getShell().input();
				Action ac = core.intercepter.intercepte(new Statement(0, 0, input));
				if (ac != null && ac.getActionName().equals("setPrompt")) {
					core.getShell().setPrompt(ac.getParams().get("prompt"));
				}
				if (ac != null && ac.getActionName().equals("resetPrompt")) {
					core.getShell().setPrompt(core.getShell().getDefaultPrompt());
				}
				if (ac != null && ac.getActionName().equals("codeWord")) {
					String nword = ac.getParams().get("narrowWord");
					NarrowWord narrowWord = StringUtil.strToNarrowWord(nword, "narrowWord");
					Word w = Code.codeWord(narrowWord, "word");
					core.getShell().getOut().println("Word:" + w.toQuadStr());
				}
				if (ac != null && ac.getActionName().equals("code")) {
					String nwords = ac.getParams().get("narrowWords");
					NarrowWords narrowWords = StringUtil.strToNarrowWords(nwords, "narrowWords");
					Words ws = Code.code(narrowWords, "words");
					if (ac.getParams().get("varName") != null) {
						Var myVar = new Var();
						myVar.setVarType("words");
						myVar.setWords(ws);
						core.putVar(ac.getParams().get("varName"), myVar);
					}
					core.getShell().getOut().println("Words:" + ws.toQuadStr());
				}
				if (ac != null && ac.getActionName().equals("decode")) {
					String varName = ac.getParams().get("varName");
					Var myvar = core.getVars().get(varName);
					Words ws = myvar.getWords();

					NarrowWords nws = Decode.decode(ws, "narrowWords");
					core.getShell().getOut().println("NarrowWords:" + nws.toHexStr());
				}
				if (ac != null && ac.getActionName().equals("monkeyTest.monkeyNarrowWord")) {
					NarrowWord narrowWord = MonkeyTest.monkeyNarrowWord("narrowWord");
					core.getShell().getOut().println("NarrowWord:" + narrowWord.toHexStr());
				}
				if (ac != null && ac.getActionName().equals("monkeyTest.monkeyWord")) {
					Word word = MonkeyTest.monkeyWord("word");
					core.getShell().getOut().println("Word:" + word.toHexStr() + ":" + word.toQuadStr());
				}
				if (ac != null && ac.getActionName().equals("quit")) {
					core.getShell().getOut().println("Byebye!");
					System.exit(0);
				}
			} catch (ValidateException ve) {
				core.getShell().getOut().println(core.getShell().getPrompt() + ve.getMessage());
			} catch (Exception e) {
				core.getShell().getOut().println(core.getShell().getPrompt() + e.getMessage());
			}
		}
	}
	
	public Intercepter getIntercepter() {
		return intercepter;
	}
	public void setIntercepter(Intercepter intercepter) {
		this.intercepter = intercepter;
	}
	public Map<String, Var> getVars() {
		return vars;
	}
	public void setVars(Map<String, Var> vars) {
		this.vars = vars;
	}
	public void putVar(String varName,Var var) {
		this.vars.put(varName, var);
	}
}
