package org.javaforever.oville;

import java.util.ArrayList;
import java.util.List;

public class NarrowWords {
	private String varName;
	private List<NarrowWord> narrowWords = new ArrayList<NarrowWord>();
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}
	public List<NarrowWord> getNarrowWords() {
		return narrowWords;
	}
	public void setNarrowWords(List<NarrowWord> narrowWords) {
		this.narrowWords = narrowWords;
	}
	public void addNarrowWord(NarrowWord nw) {
		if (this.narrowWords != null) this.narrowWords.add(nw);
		else {
			this.narrowWords = new ArrayList<NarrowWord>();
			this.narrowWords.add(nw);
		}
	}
	
	public String toHexStr() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		for (NarrowWord nw:this.getNarrowWords()) {
			sb.append(nw.toHexStr()+":");
		}
		return sb.toString();
	}	
}
