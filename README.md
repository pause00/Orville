# 奥维尔号量子计算机

#### Description
采用扩展的量子二进制算法。在经典计算机上实现量子计算机。我们的景愿是在个人计算机上实现量子霸权。
此计算机的字长是64位，等效数据位为32位字长的量子计算机。我们采用量子扩展二进制，共有４个字符:0,1,Q,P可以进行经典和量子算法。我们将在汇编层实现字符界面的量子计算机。

### 项目图片：奥维尔号
![输入图片说明](https://images.gitee.com/uploads/images/2020/1208/222730_0a228c42_1203742.png "ov2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1208/211433_353b7847_1203742.jpeg "ov.jpeg")

### 文档，更新中

### 已完成的功能
开发版中的Core和Shell已可以使用。

目前支持如下的命令

设置提示符

setPrompt Star Trek:>

重置提示符

resetPrompt

编码word

codeWord 0xffffffff

生成随机字

monkeyTest.monkeyWord

生成随即窄字

monkeyTest.monkeyNarrowWord

编码字符串

code 0x11

code 0x1111111111

退出

quit

启动Core即可启动奥维尔号。


### 操作流程【想定】

奥维尔号支持2种数据

字Word 64位扩展二进制，其实字宽为32位，每位扩展二进制占两位

窄字NarrowWord 32位二进制

奥维尔号载入数据采用窄字。

运算时将窄字codeWord成字

字可以进行量子运算

运算结果经过投影成为字的数组

运算结果字的数组会逐一进行验证，通过验证的即为结果

运算结果的字不可以包含Q和P

随后这些结果会decode成为窄字数组，即为最终的结果

### 加量子与消量子

在奥维尔号的运算中Q在投影操作中会被分支为０和１

所以结果中Q越多，结果膨胀的厉害。

这里引入加量子与消量子概念

加量子：运算结果中使Q增多的运算，可能是和Q运算或者是量子进位。

消量子：运算中使Q减少的运算，主要有如下两种

and 0

Q and 0 = 0

or 1

Q or 1 = 1



### 近期计划

实现Shell

实现Core

实现最基础的功能

使计算机可以测试

### 界面想定

Orville:> cod var a = 'myTest'

Orville:> a = XXXX

Oraville:> binShow a

Orville:> a = '0b11011101'

### 基本字符及其含义
0 经典0

1 经典1

Q 量子纠缠态

P　投影动作，会把纠缠态度分解成经典0或者1,对经典状态没有影响。

在编码时，这４个字会被编码为00,01,10和11

### 相关运算

Code 将经典字符串编码成量子扩展字符串

Decode 将量子扩展字符串中符合经典要求的字串解码为经典字符串

Project 投影运算，即加上32位宽的P

经典运算，经典字符串使用的经典运算

Qadd　量子扩展加法，为两个64位扩展字符串的运算，结果是一个64位扩展量子字符串，经投影运算，可以得到结果。

Ｑshow　显示一个量子扩展字面量，使用0,1,Q,P

Ｑbinshow  显示量子扩展字符串的二进制编码64位宽

Ｑhexshow 使用16进制显示量子扩展字符串，为16位字符串

### 量子扩展加法规则

#### 不考虑量子进位字符串
0+0 = 0

0+1 = 1

0+Q = Q

0+P = 0

1+0 = 1

1+1 = 10

1+Q = Q

1+P = 1

Q+0 = Q

Q+1 = Q

Q+Q = Q

Q+P = (0)(1)

P+0 = 0

P+1 = 1

P+Q = (0)(1)

P+P = P

#### 考虑量子进位
0+0 = 0

0+1 = 1

0+Q = Q

0+P = 0

1+0 = 1

1+1 = 10

1+Q = QQ

1+P = 1

Q+0 = Q

Q+1 = QQ

Q+Q = QQ

Q+P = (0)(1)

P+0 = 0

P+1 = 1

P+Q = (0)(1)

P+P = P


### 量子扩展位运算规则

#### and

0 and 0 = 0

0 and 1 = 0
窄字
0 and Q = 0

0 and P = 0

1 and 0 = 0

1 and 1 = 1

1 and Q = Q

1 and P = 1

Q and 0 = 0

Q and 1 = Q

Q and Q = Q

Q and P = (0)(1)

P and 0 = 0

P and 1 = 1

P and Q = (0)(1)

P and P = P

#### or
0 or 0 = 0

0 or 1 =  1

0 or Q = Q

0 or P = 0


1 or 0 = 1

1 or 1 = 1

1 or Q = 1

1 or P = 1

Q or 0 = Q

Q or 1 = 1

Q or Q = Q

Q or P = (0)(1)

P or 0 = 0

P or 1 = 1

P or Q = (0)(1)

P or P = P

#### not

not 0 = 1

not 1 = 0

not Q = Q 

not P = P

### 标准测试案例
使用扫雷游戏做为通用程序的标准测试案例。见
[https://gitee.com/jerryshensjf/JMine](https://gitee.com/jerryshensjf/JMine)


使用路径搜索算法作为NP问题标准测试床。见本站附件。






















































